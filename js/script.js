"use strict";

const btnArr = document.querySelectorAll(".btn");

document.addEventListener("keydown", e => {
    if(document.querySelector(".active")){
        document.querySelector(".active").classList.remove("active")
    }
    
    for(let i = 0; i < btnArr.length; i++){
        if(e.key.toLocaleLowerCase() === btnArr[i].textContent.toLocaleLowerCase()){
            btnArr[i].classList.add("active")
        }
    }
});

